<?php

namespace App;

use DB;
use App\Note;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

    public function notes() {
        return DB::select("SELECT notes.*,users.name FROM notes,users WHERE users.id = notes.userId AND notes.customerId= $this->id  ORDER BY datetime DESC");
    }

    public function type() {
        $type = Type::findOrFail($this->btype)->type;
        return $type;
    }

    public function statuses() {
        $statuses = Status::All();
        return $statuses;
    }

    public function positions() {
        $positions = position::All();
        return $positions;
    }

    public function accmanager() {
        $manager = User::findOrFail($this->account_manager)->name;
        return $manager;
    }

    public function position() {
        $position = '';
        if (isset($this->position) && $this->position > 0) {
            $position = position::findOrFail($this->position);
            return $position->position;
        }
        return $position;

//if($position)
    }

    public function cstatus() {
        $id = $this->id;
        $statusid = DB::select("SELECT * FROM cstatuses WHERE customerid= $id  order by datetime desc limit 1")[0]->statusid;
        $cstatus = DB::select("SELECT name FROM statuses WHERE orderid= $statusid limit 1");
        if ($cstatus)
            return $cstatus[0]->name;
        return NULL;
    }

    public function statusid() {
        $id = $this->id;
        $statusid = DB::select("SELECT * FROM cstatuses WHERE customerid= $id  order by datetime desc limit 1")[0]->statusid;
        if ($statusid)
            return $statusid;
        return NULL;
    }

}
