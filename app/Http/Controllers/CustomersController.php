<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Customer;
use App\Status;
use App\Type;
use App\User;

class CustomersController extends Controller {

    public function __construct() {
        $this->middleware('activeuser');
    }

    public function index() {
        $customers = Customer::all();
        return view('customers', compact('customers'));
    }

    public function edit($id) {
        $customer = Customer::find($id);
        $types = Type::all();
        $users = User::all();
        if ($customer) {
            return view('customer-form', compact('customer', 'types','users'));
        }
        return back();
    }

    public function show($id) {

        $customer = Customer::find($id);
        if ($customer) {
            return view('customer', compact('customer'));
        }
        return back();
    }

}
