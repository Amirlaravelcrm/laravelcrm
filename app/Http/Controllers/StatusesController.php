<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Status;

class StatusesController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $statuses = Status::all();
        return view('admin.statuses', compact('statuses'));
    }

    public function store(Request $request) {
        //var_dump($_REQUEST);
        $status = new Status;
        $status->name = $request->name;
        $status->status = $request->status;
        $status->orderid = $request->orderid;
        $status->save();
        //$position->store($_REQUEST->all());
        return back();
    }

    public function show($id) {
        $statuses = Status::all();

        $thisstatus = Status::findOrFail($id);
        return view('admin.statuses', compact('statuses'), compact('thisstatus'));
    }

    public function edit($id) {

        $status = Status::findOrFail($id);
        $status->update($_REQUEST);
        return back();
    }

    public function destroy($id) {

        $status = Status::findOrFail($id);
        $status->delete($_REQUEST);


        //$position = Position::findOrFail($id);
        //$position->update($_REQUEST);
        return redirect('admin/statuses');
    }

}
