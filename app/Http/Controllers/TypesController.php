<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Type;


class TypesController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $types = Type::all();
        return view('admin.types', compact('types'));
    }

    public function store(Request $request) {
        //var_dump($_REQUEST);
        $type = new Type;
        $type->type = $request->type;
        $type->status = $request->status;
        $type->save();
        //$position->store($_REQUEST->all());
        return back();
    }

    public function show($id) {
        $types = Type::all();

        $thistype = Type::findOrFail($id);
        return view('admin.types', compact('types'), compact('thistype'));
    }

    public function edit($id) {

        $type = Type::findOrFail($id);
        $type->update($_REQUEST);
        return back();
    }

    public function destroy($id) {

        $type = Type::findOrFail($id);
        $type->delete($_REQUEST);


        //$position = Position::findOrFail($id);
        //$position->update($_REQUEST);
        return redirect('admin/types');
    }

}
