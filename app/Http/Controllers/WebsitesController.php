<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Website;

class WebsitesController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        $websites = Website::all();
        return view('admin.websites', compact('websites'));
    }

    public function store(Request $request) {
        //var_dump($_REQUEST);
        $website = new Website;
        $website->status = $request->status;
        $website->website = $request->website;
        $website->save();
        //$position->store($_REQUEST->all());
        return back();
    }

    public function show($id) {
        $websites = Website::all();

        $thiswebsite = Website::findOrFail($id);
        return view('admin.websites', compact('websites'), compact('thiswebsite'));
    }

    public function edit($id) {

        $website = Website::findOrFail($id);
        $website->update($_REQUEST);
        return back();
    }

    public function destroy($id) {

        $website = Website::findOrFail($id);
        $website->delete($_REQUEST);


        //$position = Position::findOrFail($id);
        //$position->update($_REQUEST);
        return redirect('admin/websites');
    }

}
