<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Hash;


class AdminController extends Controller {

    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        //$users = DB::table('users')->get();
        $users = User::all();
        return view('admin.admin', compact('users'));
        //return $users;
    }

    public function edit($id) {
        $user = User::findOrFail($id);
        $users = User::all();
        return view('admin.users.edit', compact('user', 'users'));
    }

    /*
      public function update(Request $request, $id) {
      echo "here!";
      $user = User::findOrFail($id);
      //echo($user);
      //var_dump($_REQUEST);
      $user->update($_REQUEST);
      //return redirect('admin/users');
      }
     */

    public function update(Request $request, $id) {
        //echo "here!";
        $user = User::findOrFail($id);
        $_REQUEST['password'] = Hash::make($_REQUEST['password']);
        //echo($user);
        //var_dump($_REQUEST);
        $user->update($_REQUEST);
        //return redirect(url('/admin/users'));
        return redirect('admin/users');
    }

}
