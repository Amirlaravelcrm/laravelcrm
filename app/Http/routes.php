<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('postcodes', 'PostcodesController@autocomplete');


Route::group(['middleware' => ['web']], function() {
    Route::auth();

    Route::resource('/admin/users', 'AdminController');
    Route::post('/admin/users/{id}/update', 'AdminController@update');


    Route::resource('/admin/positions', 'PositionsController');
    Route::post('/admin/positions/store', 'PositionsController@store');
    Route::post('/admin/positions/edit/{id}', 'PositionsController@edit');
    Route::get('/admin/positions/destroy/{id}', 'PositionsController@destroy');

    Route::resource('/admin/websites', 'WebsitesController');
    Route::post('/admin/websites/store', 'WebsitesController@store');
    Route::post('/admin/websites/edit/{id}', 'WebsitesController@edit');
    Route::get('/admin/websites/destroy/{id}', 'WebsitesController@destroy');


    Route::resource('/admin/statuses', 'StatusesController');
    Route::post('/admin/statuses/store', 'StatusesController@store');
    Route::post('/admin/statuses/edit/{id}', 'StatusesController@edit');
    Route::get('/admin/statuses/destroy/{id}', 'StatusesController@destroy');

    Route::resource('/admin/types', 'TypesController');
    Route::post('/admin/types/store', 'TypesController@store');
    Route::post('/admin/types/edit/{id}', 'TypesController@edit');
    Route::get('/admin/types/destroy/{id}', 'TypesController@destroy');

    Route::get('/customers', 'CustomersController@index');
    Route::resource('/customer', 'CustomersController');
});




Route::any('{slug}', function($slug) {
    return view('404');
    //do whatever you want with the slug
})->where('slug', '(.*)?');
