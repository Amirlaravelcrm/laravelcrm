<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model {

    protected $fillable = [
        'type', 'status'
    ];
    
     public function customers(){
        return $this->hasMany('App\Customer');
    }
    public function select(){
        $type = Type::All();
        return $type;
    }

}
