<?PHP
//echo $thisposition;
?>
@extends('layouts.app')

@section('content')
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Details
                    <div class="btn-group" role="group">
                        <span class="btn btn-group-lg btn-default" role='group'> View</span>
                        <span class="btn btn-group-lg btn-default" role='group'> Invoicing Details</span>
                        <span class="btn btn-group-lg btn-default" role='group'> New</span>
                        <span class="btn btn-group-lg btn-default" role='group'> History</span>
                        <span class="btn btn-group-lg btn-default" role='group'> Transactions</span>
                    </div>
                </div>

            </div>
        </div>
    </div>    
</div>

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10  col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{$customer->type()}} - {{$customer->bname}} 
                    <span class="btn btn-danger btn-sm pull-right" style="margin: 0 5px;"> Remove</span>
                    <a href="{{url('/')}}/customer/{{$customer->id}}/edit">
                        <span class="btn btn-primary btn-sm pull-right" style="margin: 0 5px;">  Edit  </span>
                    </a> 
                </div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-sm-7">
                            <dl class="dl-horizontal">
                                <dt>Type</dt>
                                <dd>{{$customer->type()}}</dd>
                                <dt>Working Name</dt>
                                <dd>{{$customer->bname}}</dd>
                                <dt>Working Phone</dt>
                                <dd>{{$customer->bphone}}</dd>

                                <dt>Working Email</dt>
                                <dd><a href="mailto:{{$customer->bemail}}">{{$customer->bemail}}</a>
                                </dd>

                                <dt>ABN</dt>
                                <dd>{{$customer->abn}}
                                </dd>

                                <dt>First Name</dt>
                                <dd>{{$customer->firstname}}</dd>
                                <dt>Last Name</dt>
                                <dd>{{$customer->lastname}}</dd>
                                <dt>Personal Mobile Phone</dt>
                                <dd>{{$customer->phone}}</dd>
                                <dt>Personal Email</dt>
                                <dd><a href="mailto:{{$customer->email}}">{{$customer->email}}</a></dd>
                                <dt>Website</dt>
                                <dd>{{$customer->website}}</dd>



                                <?PHP
                                $social = (array) json_decode($customer->extend);

                                foreach ($social as $key => $value) {
                                    if ($value != '') {
                                        echo '<dt>' . $key . '</dt><dd>' . $value . '</dd>';
                                    }
                                }
                                ?>

                                <Br>
                                <dt>Address</dt>
                                <dd>{{$customer->address}}</dd>
                                <dt>Suburb</dt>
                                <dd>{{$customer->suburb}}</dd>
                                <dt>State</dt>
                                <dd>{{$customer->state}}</dd>
                                <dt>Postcode</dt>
                                <dd>{{$customer->postcode}}</dd>
                                <br>

                                <dt>Account Manager</dt>
                                <dd>{{$customer->accmanager()}}</dd>
                                @if($customer->position)
                                <dt>Contact Position</dt>
                                <dd>{{$customer->position()}}</dd>
                                @endif

                                @if($customer->mpm_user_id)
                                <dt>MPM User ID</dt>
                                <dd>{{$customer->mpm_user_id}}</dd>
                                @endif

                                @if($customer->mpm_type_id)
                                <dt>MPM {{$customer->type()}} ID</dt>
                                <dd>{{$customer->mpm_type_id}}</dd>
                                @endif

                                @if($customer->brothel_id)
                                <dt>Brothel ID</dt>
                                <dd>{{$customer->brothel_id}}</dd>
                                @endif


                            </dl>
                        </div>
                        <div class="col-sm-5">
                            <dl class="dl-horizontal">
                                <?php
                                $customerExtend = (array) json_decode($customer->additional_contacts);
                                if (isset($customerExtend["fname"])) {          // If additiopnal contact exist
                                    $count = count($customerExtend["fname"]);   // Count them
                                    echo "Additional Contacts<hr>";

                                    for ($i = 0; $i < $count; $i++) {                       // for each contact
                                        foreach ($customerExtend as $key => $values) {
                                            if ($values[$i]) {
                                                echo '<dt>' . $key . '</dt>';
                                                echo '<dd>' . $values[$i] . '</dd>';
                                            }
                                        }
                                    }
                                    echo "<hr>";
                                }
                                ?>

                            </dl>

                            <form>

                                <div class="col-md-10">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Status</div>
                                        <div class="panel-body center-block">
                                            <select class="form-control">
                                                <option disabled selected>Select...</option>
                                                @foreach($customer->statuses() as $status)
                                                @if($status->status == "Active" || $customer->statusid()==$status->orderid)
                                                <option <?= ($customer->statusid() == $status->orderid) ? "selected" : ""; ?> >{{$status->orderid}} - {{$status->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            <div class="form-inline">
                                                <div class="form-group">
                                                    <label>Due Date/Time </label>
                                                    <br>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                                        <input type="text" class="form-control" name="dueDate" value="456132" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <button class="btn btn-primary">Update</button>
                                        </div> 
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>



<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10  col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Notes
                    <button class="btn btn-primary pull-right">Save</button>
                </div>
                <div class="panel-body center-block">
                    <textarea id="txtEditor"></textarea>


                </div>           
            </div>
        </div>
    </div>    
</div>

@foreach($customer->notes() as $note)
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{$note->datetime}} - {{$note->name}}




                    <span class="glyphicon glyphicon-remove btn btn-danger btn-sm pull-right" style="margin: 0 5px;"></span>
                    <a href="{{url('/')}}/customer/{{$customer->id}}/edit">
                        <span class="glyphicon glyphicon-pencil btn btn-primary btn-sm pull-right" style="margin: 0 5px;"></span>
                    </a> 


                </div>
                <div class="panel-body center-block">
                    <?= $note->note ?>
                </div>           
            </div>
        </div>
    </div>    
</div>
@endforeach



@endsection
