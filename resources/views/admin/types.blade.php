<?PHP
//echo $thisposition;
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Primary Admin Dashboard</div>
            </div>
        </div>
    </div>    
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Types List</div>
                <div class="panel-body">

                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Types</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($types as $type)
                            <tr>
                                <td>
                                    <a href="{{url('/admin/types/')}}/{{$type->id}}"> {{$type->type}}</a></td>
                                <td><span class="glyphicon glyphicon-stop" id="{{$type->status}}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add New type</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thistype->id)) ? url('/admin/types/edit/' . $thistype->id) : url('/admin/types/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">type</label>

                            <div class="col-md-6">
                                <input id="position" type="text" class="form-control" name="type" value="{{{ $thistype->type or '' }}}">

                                @if ($errors->has('type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>                            
                            <div class="col-md-6">
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Active" <?= (isset($thistype) && $thistype->status === "Active" ) ? "checked" : ""; ?>> Active<br>
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Deactive" <?= (isset($thistype) && $thistype->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>


                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> 
                                    <?PHP
                                    echo (isset($thistype->id)) ? "Update Type" : "Register New Type";
                                    ?>
                                </button>              
                                <?PHP
                                if (isset($thistype->id)) {
                                    ?>

                                    <a type="delete" class="btn btn-danger sweetDeleteButton" link="{{url('/admin/types/destroy/' . $thistype->id)}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-remove"></i>Delete                                    
                                    </a>

                                    <a type="delete" class="btn btn-success" href="{{url('/admin/types')}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-backward"></i>Cancle                                    
                                    </a>

                                    <?PHP
                                }
                                ?>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
