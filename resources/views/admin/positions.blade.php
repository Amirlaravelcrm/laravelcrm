<?PHP
//echo $thisposition;
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Primary Admin Dashboard</div>
            </div>
        </div>
    </div>    
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Positions List</div>
                <div class="panel-body">

                        <table id="users_list" class="table table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <td>positions</td>
                                    <td>status</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($positions as $position)
                                <tr>
                                    <td>
                                        </span><a href="{{url('/admin/positions/')}}/{{$position->id}}"> {{$position->position}}</a></td>
                                    <td><span class="glyphicon glyphicon-stop" id="{{$position->status}}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>


                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Position</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thisposition->id)) ? url('/admin/positions/edit/' . $thisposition->id) : url('/admin/positions/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                            <label for="position" class="col-md-4 control-label">Position</label>

                            <div class="col-md-6">
                                <input id="position" type="text" class="form-control" name="position" value="{{{ $thisposition->position or '' }}}">

                                @if ($errors->has('position'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('position') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>                            
                            <div class="col-md-6">
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Active" <?= (isset($thisposition) && $thisposition->status === "Active" ) ? "checked" : ""; ?>> Active<br>
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Deactive" <?= (isset($thisposition) && $thisposition->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>


                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> 
                                    <?PHP
                                    echo (isset($thisposition->id)) ? "Update Position" : "Register New Position";
                                    ?>
                                </button>              
                                <?PHP
                                if (isset($thisposition->id)) {
                                    ?>

                                    <a type="delete" class="btn btn-danger sweetDeleteButton" link="{{url('/admin/positions/destroy/' . $thisposition->id)}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-remove"></i>Delete                                    
                                    </a>

                                    <a type="delete" class="btn btn-success" href="{{url('/admin/positions')}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-backward"></i>Cancle                                    
                                    </a>

                                    <?PHP
                                }
                                ?>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
