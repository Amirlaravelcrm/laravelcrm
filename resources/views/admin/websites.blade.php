<?PHP
//echo $thisposition;
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Primary Admin Dashboard</div>
            </div>
        </div>
    </div>    
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Websites List</div>
                <div class="panel-body">

                    <table id="users_list" class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <td>Websites</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($websites as $website)
                            <tr>
                                <td>
                                    </span><a href="{{url('/admin/websites/')}}/{{$website->id}}"> {{$website->website}}</a></td>
                                <td><span class="glyphicon glyphicon-stop" id="{{$website->status}}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Website</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thiswebsite->id)) ? url('/admin/websites/edit/' . $thiswebsite->id) : url('/admin/websites/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="col-md-4 control-label">Website</label>

                            <div class="col-md-6">
                                <input id="position" type="text" class="form-control" name="website" value="{{{ $thiswebsite->website or '' }}}">

                                @if ($errors->has('website'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('website') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>                            
                            <div class="col-md-6">
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Active" <?= (isset($thiswebsite) && $thiswebsite->status === "Active" ) ? "checked" : ""; ?>> Active<br>
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Deactive" <?= (isset($thiswebsite) && $thiswebsite->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>


                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> 
                                    <?PHP
                                    echo (isset($thiswebsite->id)) ? "Update Website" : "Register New Website";
                                    ?>
                                </button>              
                                <?PHP
                                if (isset($thiswebsite->id)) {
                                    ?>

                                    <a type="delete" class="btn btn-danger sweetDeleteButton" link="{{url('/admin/websites/destroy/' . $thiswebsite->id)}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-remove"></i>Delete                                    
                                    </a>

                                    <a type="delete" class="btn btn-success" href="{{url('/admin/websites')}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-backward"></i>Cancle                                    
                                    </a>

                                    <?PHP
                                }
                                ?>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
