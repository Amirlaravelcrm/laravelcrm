<?PHP
//echo $thisposition;
?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Primary Admin Dashboard</div>
            </div>
        </div>
    </div>    
</div>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Statuses List</div>
                <div class="panel-body">

                        <table id="users_list" class="table table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <td>Order</td>
                                    <td>Status</td>
                                    <td>Active</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($statuses as $status)
                                <tr>

                                    <td>
                                        {{$status->orderid}}
                                    </td>
                                    <td>
                                        <a href="{{url('/admin/statuses/')}}/{{$status->id}}"> {{$status->name}}</a>
                                    </td>
                                    <td>
                                        <span class="glyphicon glyphicon-stop" id="{{$status->status}}"></span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>


                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Status</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action=
                    <?PHP
                    echo (isset($thisstatus->id)) ? url('/admin/statuses/edit/' . $thisstatus->id) : url('/admin/statuses/store');
                    ?>>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="orderid" class="col-md-4 control-label">Order id</label>

                            <div class="col-md-6">
                                <input id="orderid" type="text" class="form-control" name="orderid" value="{{{ $thisstatus->orderid or '' }}}">

                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('orderid') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>    
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                <input id="position" type="text" class="form-control" name="name" value="{{{ $thisstatus->name or '' }}}">

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Active</label>                            
                            <div class="col-md-6">
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Active" <?= (isset($thisstatus) && $thisstatus->status === "Active" ) ? "checked" : ""; ?>> Active<br>
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Deactive" <?= (isset($thisstatus) && $thisstatus->status === "Deactive" ) ? "checked" : ""; ?>> Deactive<br>


                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> 
                                    <?PHP
                                    echo (isset($thisstatus->id)) ? "Update Status" : "Register New Status";
                                    ?>
                                </button>              
                                <?PHP
                                if (isset($thisstatus->id)) {
                                    ?>

                                    <a type="delete" class="btn btn-danger sweetDeleteButton" link="{{url('/admin/statuses/destroy/' . $thisstatus->id)}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-remove"></i>Delete                                    
                                    </a>

                                    <a type="delete" class="btn btn-success" href="{{url('/admin/statuses')}}">
                                        <i class=" fa  fa-btn glyphicon glyphicon-backward"></i>Cancle                                    
                                    </a>

                                    <?PHP
                                }
                                ?>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
