@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Primary Admin Dashboard</div>
            </div>
        </div>
    </div>    
</div>



<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit User</div>
                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{url('/admin/users/'.$user->id.'/update')}}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}">

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>                            
                            <div class="col-md-6">
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Active" <?= ($user->status == "Active" ) ? "checked" : ""; ?>> Active<br>
                                <input id="status" type="radio" class="col-md-2"  name="status" value="Deactive" <?= ($user->status == "Deactive" ) ? "checked" : ""; ?>> Deactive<br>
                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                            <label for="group_id" class="col-md-4 control-label">User Level</label>                            
                            <div class="col-md-6">
                                <input id="status" type="radio" class="col-md-2"  name="group_id" value="Admin" <?= ($user->group_id == "Admin" ) ? "checked" : ""; ?>> Admin<br>
                                <input id="status" type="radio" class="col-md-2"  name="group_id" value="User" <?= ($user->group_id == "User" ) ? "checked" : ""; ?>> User<br>
                            </div>
                        </div>
                        
                        
                        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Tasks assigned to</label>                           
                            <div class="col-md-6">
                                <select class="form-control" name="assignedto">
                                    <option value="" disabled selected>select...</option>
                                    @foreach($users as $assignto)
                                    @if($assignto->status!="Deactive")
                                    <option value="{{$assignto->id}}" <?= ($user->assignedto == $assignto->id) ? 'selected' : ''; ?>>{{$assignto->name}}<?= ($user->id == $assignto->id) ? ' - SELF' : ''; ?></option>
                                    @endif
                                    @endforeach

                                </select>
                            </div>
                        </div>










                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
