@extends('layouts.app')

@section('content')
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body" style="display: none"></div>

            </div>
        </div>
    </div>    
</div>

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customers List
                    <button class="glyphicon glyphicon-download-alt btn btn-primary pull-right"> Export</button>
                </div>
                <div class="panel-body">

                    <table id="customers_list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Suburb</th>
                                <th>State</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                            </tr>
                        </thead> 

                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Suburb</th>
                                <th>State</th>
                                <th>Acc Manager</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{$customer->id}}</td>
                                <td>{{$customer->type()}}</td>
                                <td><a href="{{url('/customer/')}}/{{$customer->id}}"> {{$customer->bname}}</a></td>
                                <td>{{$customer->bphone}}</td>
                                <td>{{$customer->suburb}}</td>
                                <td>{{$customer->state}}</td>
                                <td>{{$customer->accmanager()}}</td>
                                <td>{{$customer->cstatus()}}</td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>




@endsection
