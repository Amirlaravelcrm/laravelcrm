<?PHP

//echo $thisposition;
use App\Type;
use App\Address;
?>


@extends('layouts.app')

@section('content')
<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="btn-group" role="group">Edit Customer
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

<div class="fullcontainer">
    <div class="row">
        <div class="col-md-10  col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Details From
                
                
                
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/') }}">
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="dl-horizontal">



                                    {{-- ******* Working Name   ************ --}}
                                    <div class="form-group{{ $errors->has('bname') ? ' has-error' : '' }}">
                                        <label for="bname" class="col-md-4 control-label">Business Name</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->bname or ''}}" name="bname">
                                            @if ($errors->has('bname'))
                                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- **********  Working Phone ************** --}}
                                    <div class="form-group{{ $errors->has('bphone') ? ' has-error' : '' }}">
                                        <label for="bphone" class="col-md-4 control-label">Business Phone</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->bphone or ''}}" name="bphone">
                                            @if ($errors->has('bphone'))
                                            <span class="help-block"><strong>{{ $errors->first('bphone') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- ********  Working Email  ************** --}}
                                    <div class="form-group{{ $errors->has('bemail') ? ' has-error' : '' }}">
                                        <label for="bemail" class="col-md-4 control-label">Business Email</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->bemail or ''}}" name="bemail">
                                            @if ($errors->has('bemail'))
                                            <span class="help-block"><strong>{{ $errors->first('bemail') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- ********  Working Abn  ************** --}}
                                    <div class="form-group{{ $errors->has('abn') ? ' has-error' : '' }}">
                                        <label for="abn" class="col-md-4 control-label">ABN Number</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->abn or ''}}" name="abn">
                                            @if ($errors->has('abn'))
                                            <span class="help-block"><strong>{{ $errors->first('abn') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- *******  First Name  ************** --}}
                                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                        <label for="firstname" class="col-md-4 control-label">First Name</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->firstname or ''}}" name="firstname">
                                            @if ($errors->has('firstname'))
                                            <span class="help-block"><strong>{{ $errors->first('firstname') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- *******  Last Name  ************** --}}
                                    <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                        <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->lastname or ''}}" name="lastname">
                                            @if ($errors->has('lastname'))
                                            <span class="help-block"><strong>{{ $errors->first('lastname') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- *******  Personal Mobile Phone ************** --}}
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="col-md-4 control-label">Contact Phone</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->phone or ''}}" name="phone">
                                            @if ($errors->has('phone'))
                                            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- *******  Contact Position ************** --}}
                                    <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                        <label for="position" class="col-md-4 control-label">Contact Position</label>
                                        <div class="col-md-6">
                                            <select class="form-control" name="position">
                                                <option></option>
                                                @foreach($customer->positions() as $position)
                                                <option value="{{$position->id}}" <?= ($position->id == $customer->position) ? 'selected' : ''; ?>>{{$position->position}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('position'))
                                            <span class="help-block"><strong>{{ $errors->first('position') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>

                                    {{-- *******  Personal Email ************** --}}
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">Personal Email</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->email or ''}}" name="email">
                                            @if ($errors->has('email'))
                                            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- *******  Website ************** --}}
                                    <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                                        <label for="website" class="col-md-4 control-label">Website</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->website or ''}}" name="website">
                                            @if ($errors->has('website'))
                                            <span class="help-block"><strong>{{ $errors->first('website') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>


                                    <?PHP
                                    $social = (array) json_decode($customer->extend);
                                    ?>
                                    {{-- *******  Social  ************** --}}
                                    <div class="form-group">
                                        <label for="facebook" class="col-md-4 control-label">Facebook</label>
                                        <div class="col-md-6">
                                            <input name="facebook" type="text" placeholder="facebook" id="facebook" class="form-control" value="{{$social ['facebook'] or ''}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="twitter" class="col-md-4 control-label">Twitter</label>
                                        <div class="col-md-6">
                                            <input name="twitter" type="text" placeholder="twitter" id="facebook" class="form-control" value="{{$social ['twitter'] or ''}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="instagram" class="col-md-4 control-label">Instagram</label>
                                        <div class="col-md-6">
                                            <input name="instagram" type="text" placeholder="instagram" id="instagram" class="form-control" value="{{$social ['instagram'] or ''}}">
                                        </div>
                                    </div>
                                    {{-- *******  Address ************** --}}
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label for="address" class="col-md-4 control-label">Address</label>
                                        <div class="col-md-6">
                                            <input class="form-control" value="{{$customer->address or ''}}" name="address">
                                            @if ($errors->has('address'))
                                            <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- *******  search postcode  ************** --}}
                                    <div class="form-group">
                                        <label for="lastname" class="col-md-4 control-label">Postcode Lookup</label>
                                        <div class="col-md-6">
                                            <input type="text" placeholder="Search Suburb or Postcode" id="SearchPostcodes" class="form-control" autocomplete="off">
                                            <input disabled type="text" placeholder="Search Suburb or Postcode" id="LockSearch" class="form-control" value="{{$customer->postcode}}, {{$customer->suburb}}, {{$customer->state}}" autocomplete="off">

                                            <input style="display: none" name="postcode" type="text" id="postcode" class="form-control" value="{{$customer->postcode}}">
                                            <input style="display: none" name="suburb" type="text" id="suburb" class="form-control" value="{{$customer->suburb}}">
                                            <input style="display: none" name="state" type="text" id="state" class="form-control" value="{{$customer->state}}">


                                        </div>
                                    </div>


                                </dl>




                            </div>
                            <div class="col-sm-6">

                                <dl class="dl-horizontal">
                                    <div>Additional Contacts
                                        <span id="addContactButton" class="btn btn-primary glyphicon glyphicon-plus pull-right"></span>
                                    </div>
                                    <br>
                                    {{-- convert PHP array tp javascript array --}}
                                    <script type="text/javascript">var extContact = <?= $customer->additional_contacts ?>;</script>
                                    <div id="additionalContactsWrapper">
                                        <ol>
                                        </ol>                                        
                                    </div>
                                    <hr>
                                    {{-- *******  Business Type  ********* --}}
                                    <div class="form-group{{ $errors->has('btype') ? ' has-error' : '' }}">
                                        <label for="btype" class="col-md-4 control-label">Business Type</label>
                                        <div class="col-md-6"> 
                                            <select id="businessTypeSelect" class="form-control" name="btype">
                                                <option selected disabled>Select...</option>
                                                @foreach($types as $type)
                                                <option value="{{$type->id}}" <?= ($type->id == $customer->btype) ? 'selected' : ''; ?>>{{$type->type}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('btype'))
                                            <span class="help-block"><strong>{{ $errors->first('btype') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>


                                    {{-- *******  Acount Manager  ********* --}}
                                    <div class="form-group">
                                        <label for="account_manager" class="col-md-4 control-label">Account Manager</label>
                                        <div class="col-md-6"> 
                                            <select class="form-control" name="account_manager">
                                                <option selected disabled>Select...</option>
                                                @foreach($users as $user)
                                                <option value="{{$user->id}}" <?= ($user->id == $customer->account_manager) ? 'selected' : ''; ?>>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label for="mpm_user_id" class="col-md-4 control-label">MPM User ID</label>
                                        <div class="col-md-3">
                                            <input name="mpm_user_id" type="text" placeholder="MPM user id" id="instagram" class="form-control" value="{{$customer->mpm_user_id or ''}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mpm_type_id" class="col-md-4 control-label">MPM <span id="SelectedBusinessType"></span> ID</label>
                                        <div class="col-md-3">
                                            <input name="mpm_type_id" type="text" placeholder="MPM type id" id="instagram" class="form-control" value="{{$customer->mpm_type_id or ''}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="brothel_id" class="col-md-4 control-label">Brothel ID</label>
                                        <div class="col-md-3">
                                            <input name="brothel_id" type="text" placeholder="Brothel ID" id="instagram" class="form-control" value="{{$customer->brothel_id or ''}}">
                                        </div>
                                    </div>

                                </dl>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<div class="addContactTemplate" style="display:none;">
    <li>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Add Constact
                    <span id="removeBurron" class="btn btn-xs btn-danger glyphicon glyphicon-minus pull-right" onclick="removeContact()"></span>
                </div>
                <div class="panel-body center-block">
                    <div class="form-group">
                        <div class="col-md-4"><input id="fname" name="fname" class="form-control" placeholder="first name"></div>
                        <div class="col-md-4"><input id="lname" name="lname" class="form-control" placeholder="last name"></div>
                        <div class="col-md-4"><input id="phone" name="phone" class="form-control" placeholder="phone"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <select class="form-control" name="position">
                                <option disabled selected value="">Position...</option>
                                <option></option>
                                @foreach($customer->positions() as $position)
                                <option value="{{$position->id}}">{{$position->position}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-8"><input id="email" name="email" class="form-control" placeholder="email"></div>
                    </div>
                </div>
            </div>
        </div>
    </li>
</div>
